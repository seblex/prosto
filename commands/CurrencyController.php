<?php

namespace app\commands;

use app\models\Currency;
use yii\console\Controller;
use yii\console\ExitCode;
use linslin\yii2\curl;
use yii\db\Exception;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class CurrencyController extends Controller
{
    /**
     *
     */
    public function actionUpdate()
    {
        $curl = new curl\Curl();

        $response = $curl->get('http://www.cbr.ru/scripts/XML_daily.asp');

        if ($curl->errorCode === null) {
            $xml = simplexml_load_string($response);

            foreach($xml as $k => $v) {
                if ($c = Currency::find()->where(['name' => $v->CharCode])->one()) {
                    $c->rate = $v->Value;
                    if ($c->save()) {
                        echo 'Информация о стоимости ' .  $v->Name . ' обновлена' . PHP_EOL;
                    } else {
                        throw new Exception('Ошибка обновления валюты');
                    }
                } else {
                    $c = new Currency();
                    $c->name = $v->CharCode;
                    $c->rate = $v->Value;
                    if ($c->save()) {
                        echo 'Информация о стоимости ' .  $v->Name . ' сохранена' . PHP_EOL;
                    } else {
                        throw new Exception('Ошибка сохранения валюты');
                    }
                }
            }
            echo 'Обновление пула валют прошло успешно :)' . PHP_EOL;
        } else {
            throw new Exception('Ошибка запроса в API центрального банка');
        }
        return ExitCode::OK;
    }
}