<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m181203_142621_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string('32')->notNull()->comment('название валюты'),
            'rate' => $this->string(32)->notNull()->comment('курс валюты к рублю')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
