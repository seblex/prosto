<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Currency;
use yii\filters\auth\HttpBearerAuth;

class CurrencyController extends ActiveController

{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only'] = ['currencies', 'currency'];
        return $behaviors;
    }

    public $modelClass = 'app\models\Currency';

    public function actionIndex()
    {
        $currencies = Currency::find()->all();
        return $this->asJson($currencies);
    }
}
?>